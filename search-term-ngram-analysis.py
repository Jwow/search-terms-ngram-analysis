from google.ads.googleads.client import GoogleAdsClient
import gspread

# Google Ads Client Account ID
account_id = 'XXX-XXX-XXXX'

# Google Sheets Key
SPREADSHEET_KEY = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX'

# Define Report Date Range
start_date = 'YYYY-MM-DD'
end_date = 'YYYY-MM-DD'

# Define Ngram Count
ngrams = 2

def get_search_terms_report(client, customer_id, start_date, end_date):
    """Get a Google Ads search term report

    Args:
        client: initialized google ads client
        customer_id: account id
        start_date: format 'YYYY-MM-DD'
        end_date: format 'YYYY-MM-DD'

    Returns:
        List of search term dictionaries
    """
    ga_service = client.get_service("GoogleAdsService")

    results = []

    search_terms_query = f"""
                        SELECT
                          search_term_view.search_term,
                          ad_group.name,
                          campaign.name,
                          metrics.clicks,
                          metrics.impressions,
                          metrics.conversions,
                          metrics.cost_micros,
                          metrics.conversions_value
                        FROM search_term_view
                        WHERE segments.date BETWEEN '{start_date}' AND '{end_date}'
                        AND metrics.clicks > 0"""

    search_request = client.get_type("SearchGoogleAdsRequest")
    search_request.customer_id = customer_id
    search_request.query = search_terms_query

    response = ga_service.search(request=search_request)
    for row in response:
        result = {
            'Search Term': row.search_term_view.search_term,
            'Campaign': row.campaign.name,
            'Ad Group': row.ad_group.name,
            'Clicks': row.metrics.clicks,
            'Impressions': row.metrics.impressions,
            'Conversions': row.metrics.conversions,
            'Cost': float('{:.2f}'.format(row.metrics.cost_micros / 1000000)),
            'Conversion Value': row.metrics.conversions_value

        }
        results.append(result)

    return results


def create_ngram_dict(report, ngram_count):
    ngram_dict = {}

    for row in report:
        term = row['Search Term']
        # split the term into a list of it's component words, and then create a list of the term's ngrams
        # by recombining the words in sequence given the ngram count
        words = term.split()
        ngrams = [" ".join(words[i:i + ngram_count]) for i in range(len(words) - (ngram_count - 1))]

        for ngram in ngrams:
            if ngram in ngram_dict:
                ngram_dict[ngram]['cost'] += row['Cost']
                ngram_dict[ngram]['conversions'] += row['Conversions']
                ngram_dict[ngram]['count'] += 1
                ngram_dict[ngram]['clicks'] += row['Clicks']
                ngram_dict[ngram]['impressions'] += row['Impressions']
                ngram_dict[ngram]['conversion_value'] += row['Conversion Value']
            # if the ngram isn't already in the dictionary, add an entry for it
            else:
                ngram_dict[ngram] = {'count': 1,
                                     'cost': row['Cost'],
                                     'conversions': row['Conversions'],
                                     'clicks': row['Clicks'],
                                     'impressions': row['Impressions'],
                                     'conversion_value': row['Conversion Value']}

    # calculate desired additional metrics
    calculate_metric(ngram_dict, 'cost', 'conversions', 'cost_per_conversion')
    calculate_metric(ngram_dict, 'conversion_value', 'cost', 'roas')
    calculate_metric(ngram_dict, 'clicks', 'impressions', 'ctr')

    return ngram_dict


def calculate_metric(any_dict, metric1, metric2, new_metric_name):
    for item in any_dict:
        if any_dict[item][metric2] == 0:
            any_dict[item][new_metric_name] = "{:.2f}".format(0)
        else:
            operation = any_dict[item][metric1] / any_dict[item][metric2]
            any_dict[item][new_metric_name] = float('{:.2f}'.format(operation))
    return any_dict


def convert_to_list_of_lists(ngram_dict):
    # converts the dictionary of ngrams to a list of lists for writing to Google Sheets

    # headers = ['Search Phrase'] + list(next(iter(ngram_dict.values())).keys())
    # results = [headers]
    # results.extend([[ngram] + list(row.values()) for ngram, row in ngram_dict.items()])

    # Setting the order and formatting of the columns manually
    results = [["Search Phrase", "Count", "Total Clicks", "Total Impressions", "CTR", "Total Cost", "Total Conversions",
                "Conversion Value", "Cost/Conversion", "ROAS"]]
    for ngram in ngram_dict:
        results.append([ngram, ngram_dict[ngram]['count'], ngram_dict[ngram]['clicks'],
                        ngram_dict[ngram]['impressions'], float('{:.4f}'.format(float(ngram_dict[ngram]['ctr']))),
                        float('{:.2f}'.format(ngram_dict[ngram]['cost'])), ngram_dict[ngram]['conversions'],
                        float('{:.2f}'.format(ngram_dict[ngram]['conversion_value'])),
                        float('{:.2f}'.format(float(ngram_dict[ngram]['cost_per_conversion']))),
                        float(ngram_dict[ngram]['roas'])
                       ])
    return results


def write_to_sheets(list_set, sheet_name, spreadsheet_key):
    gc = gspread.oauth(authorized_user_filename='authorized_user.json')
    sh = gc.open_by_key(spreadsheet_key)
    worksheet = sh.add_worksheet(title=sheet_name, rows="100", cols="20")
    worksheet.append_rows(list_set)


def main(client, customer_id, ngram_count, sheet_name, sheet_key):
    report = get_search_terms_report(client, customer_id, start_date, end_date)
    ngrams = create_ngram_dict(report, ngram_count)
    write_to_sheets(convert_to_list_of_lists(ngrams), sheet_name, sheet_key)


if __name__ == "__main__":
    # GoogleAdsClient will read the google-ads.yaml configuration file in the home directory if none is specified.
    googleads_client = GoogleAdsClient.load_from_storage(version="v14")
    main(googleads_client, account_id, ngrams, "Sheet Name", SPREADSHEET_KEY)
    